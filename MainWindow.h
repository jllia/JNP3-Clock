#include <windows.h>
#include <wincodec.h>
#include <windowsx.h>
#include <iostream>
#include <iomanip>
#include <numbers>
#include <cmath>
#include <d2d1.h>
#include <iostream>
#include <stdlib.h>
#include <utility>
#include <vector>   
#include <array>

#pragma comment(lib, "d2d1")

#include "basewin.h"
using D2D1::BitmapProperties;
using D2D1::PixelFormat;
using D2D1::Point2F;
using D2D1::Point2U;
using D2D1::RectF;
using D2D1::RectU;
using D2D1::SizeU;
using D2D1::Matrix3x2F;
using std::sin;
using std::array;

class MainWindow : public BaseWindow<MainWindow> {
    ID2D1Factory* pFactory;
    ID2D1HwndRenderTarget* pRenderTarget;
    ID2D1PathGeometry* nose_Geometry;
    ID2D1PathGeometry* body_Geometry;
    ID2D1PathGeometry* smile_Geometry;
    ID2D1SolidColorBrush* pBrush;
    ID2D1SolidColorBrush* pNoseBrush;

    ID2D1Bitmap* watchBitmap = nullptr;
    ID2D1Bitmap* timeBitmap = nullptr;

    IWICImagingFactory* m_pIWICFactory;

    ID2D1RadialGradientBrush* pBodyBrush = nullptr;
    ID2D1RadialGradientBrush* pEyeBrush = nullptr;

    ID2D1GradientStopCollection* rad_stops = nullptr;
    D2D1_GRADIENT_STOP rad_stops_data[3];

    ID2D1GradientStopCollection* eye_stops = nullptr;
    D2D1_GRADIENT_STOP eye_stops_data[2];

    D2D1::Matrix3x2F transformation;

    float cen_x = 0;
    float cen_y = 0;

    float digit_x = 108;
    float digit_y = 192;

    bool show_dot = true;
    int count = 0;

    int min1 = 0;
    int min2 = 0;
    int h1 = 0;
    int h2 = 0;

    float angle = 5;

    void    CalculateLayout();
    HRESULT MainWindow::LoadBitmapFromFile(
        ID2D1RenderTarget* pRenderTarget,
        IWICImagingFactory* pIWICFactory,
        ID2D1Bitmap** pBitMap,
        PCWSTR uri,
        UINT destinationWidth,
        UINT destinationHeight
    );
    HRESULT CreateGraphicsResources();
    void    DiscardGraphicsResources();
    void    OnPaint();
    void    Resize();

public:

    MainWindow() : pFactory(nullptr), pRenderTarget(nullptr), pBrush(nullptr)
    {
    }

    PCWSTR  ClassName() const { return L"Clock Window Class"; }
    LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
};
