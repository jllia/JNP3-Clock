#include "MainWindow.h"

UINT const bmp_width = 2;
UINT const bmp_height = 2;

D2D1_COLOR_F const background_color =
{ 0.55f, 0.35f,  0.70f, 1.0f };

template <class T> void SafeRelease(T** ppT) {
    if (*ppT)
    {
        (*ppT)->Release();
        *ppT = nullptr;
    }
}

void MainWindow::CalculateLayout() {
    if (pRenderTarget != nullptr) {
        D2D1_SIZE_F size = pRenderTarget->GetSize();
        cen_x = size.width / 2;
        cen_y = size.height / 2;
    }
}

HRESULT MainWindow::LoadBitmapFromFile(
    ID2D1RenderTarget* pRenderTarget,
    IWICImagingFactory* pIWICFactory,
    ID2D1Bitmap** pBitMap,
    PCWSTR uri,
    UINT destinationWidth,
    UINT destinationHeight
)
{
    IWICBitmapDecoder* pDecoder = nullptr;
    IWICBitmapFrameDecode* pSource = nullptr;
    IWICStream* pStream = nullptr;
    IWICFormatConverter* pConverter = nullptr;
    IWICBitmapScaler* pScaler = nullptr;

    HRESULT hr = pIWICFactory->CreateDecoderFromFilename(
        uri,
        nullptr,
        GENERIC_READ,
        WICDecodeMetadataCacheOnLoad,
        &pDecoder
    );

    hr = pDecoder->GetFrame(0, &pSource);

    hr = pIWICFactory->CreateFormatConverter(&pConverter);

    hr = pConverter->Initialize(
        pSource,
        GUID_WICPixelFormat32bppPBGRA,
        WICBitmapDitherTypeNone,
        nullptr,
        0.f,
        WICBitmapPaletteTypeMedianCut
    );
    {

        // Create a Direct2D bitmap from the WIC bitmap.
        hr = pRenderTarget->CreateBitmapFromWicBitmap(
            pConverter,
            nullptr,
            pBitMap
        );

        SafeRelease(&pDecoder);
        SafeRelease(&pSource);
        SafeRelease(&pStream);
        SafeRelease(&pConverter);
        SafeRelease(&pScaler);

        return hr;
    }
}

HRESULT MainWindow::CreateGraphicsResources() {
    HRESULT hr = S_OK;
    if (pRenderTarget == nullptr) {
        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

        hr = pFactory->CreateHwndRenderTarget(
            D2D1::RenderTargetProperties(),
            D2D1::HwndRenderTargetProperties(m_hwnd, size),
            &pRenderTarget);

        hr = CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);

        hr = CoCreateInstance(
            CLSID_WICImagingFactory,
            nullptr,
            CLSCTX_INPROC_SERVER,
            __uuidof(IWICImagingFactory),
            reinterpret_cast<LPVOID*>(&m_pIWICFactory)
        );

        if (SUCCEEDED(hr)) {
            hr = pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(0.0f, 0.0f, 0), &pBrush);
            hr = pRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::DimGray), &pNoseBrush);


            hr = LoadBitmapFromFile(
                pRenderTarget,
                m_pIWICFactory, &timeBitmap,
                L"Digits.png", bmp_width, bmp_height
            );

            hr = LoadBitmapFromFile(
                pRenderTarget,
                m_pIWICFactory, &watchBitmap,
                L"Watch.png", bmp_width, bmp_height
            );



            if (SUCCEEDED(hr)) {
                CalculateLayout();
            }
        }

    }
    return hr;
}

void MainWindow::DiscardGraphicsResources() {
    SafeRelease(&pRenderTarget);
    SafeRelease(&pBrush);
}

void MainWindow::OnPaint() {
    HRESULT hr = CreateGraphicsResources();
    if (SUCCEEDED(hr)) {

        pRenderTarget->BeginDraw();

        pRenderTarget->Clear(background_color);

        pRenderTarget->SetTransform(
            Matrix3x2F::Rotation(-angle, Point2F(350, 250))
        );


        pRenderTarget->DrawBitmap(
            watchBitmap,
            RectF(cen_x - 400, cen_y - 200, cen_x + 400, cen_y + 200),
            1.0f,
            D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR);

        RectF(100, 50, 850, 450),


            pRenderTarget->DrawBitmap(
                timeBitmap,
                RectF(cen_x - 280, cen_y - 100, cen_x - 172, cen_y + 92),
                0.7f,
                D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
                RectF(0 + h2 * digit_x, 00, (h2 + 1) * digit_x, digit_y));

        pRenderTarget->DrawBitmap(
            timeBitmap,
            RectF(cen_x - 280 + digit_x, cen_y - 100, cen_x - 172 + digit_x, cen_y + 92),
            0.7f,
            D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
            RectF(0 + h1 * digit_x, 00, (h1 + 1) * digit_x, digit_y));

        if (show_dot) {
            pRenderTarget->DrawBitmap(
                timeBitmap,
                RectF(cen_x - 290 + 2 * digit_x, cen_y - 100, cen_x - 182 + 2 * digit_x, cen_y + 92),
                0.7f,
                D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
                RectF(0 + timeBitmap->GetSize().width - 100, 00, 0 + timeBitmap->GetSize().width, digit_y));
        }

        pRenderTarget->DrawBitmap(
            timeBitmap,
            RectF(cen_x - 290 + 100 + 2 * digit_x, cen_y - 100, cen_x - 182 + 100 + 2 * digit_x, cen_y + 92),
            0.7f,
            D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
            RectF(0 + min2 * digit_x, 00, (min2 + 1) * digit_x, digit_y));

        pRenderTarget->DrawBitmap(
            timeBitmap,
            RectF(cen_x - 290 + 100 + 3 * digit_x, cen_y - 100, cen_x - 182 + 100 + 3 * digit_x, cen_y + 92),
            0.7f,
            D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR,
            RectF(0 + min1 * digit_x, 00, 0 + (min1 + 1) * digit_x, digit_y));


        hr = pRenderTarget->EndDraw();
        if (FAILED(hr) || hr == D2DERR_RECREATE_TARGET)
        {
            DiscardGraphicsResources();
        }
        ValidateRect(m_hwnd, nullptr);

        // EndPaint(m_hwnd, &ps);
    }
}

void MainWindow::Resize() {
    if (pRenderTarget != nullptr) {
        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

        pRenderTarget->Resize(size);
        CalculateLayout();
        InvalidateRect(m_hwnd, nullptr, FALSE);
    }
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow)
{
    MainWindow win;

    if (!win.Create(L"Clock", WS_OVERLAPPEDWINDOW)) {
        return 0;
    }

    ShowWindow(win.Window(), nCmdShow);

    // Run the message loop.

    MSG msg = { };
    while (GetMessage(&msg, nullptr, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    switch (uMsg)
    {
    case WM_CREATE:
        SetTimer(m_hwnd, 1000, 100, nullptr);
        if (FAILED(D2D1CreateFactory(
            D2D1_FACTORY_TYPE_SINGLE_THREADED, &pFactory))) {
            return -1;  // Fail CreateWindowEx.
        }
        srand(time(0));
        min1 = rand() % 10;
        min2 = rand() % 6;
        h2 = rand() % 3;
        if (h2 == 2) {
            h1 = rand() % 4;
        }
        else {
            h1 = rand() % 10;
        }
        return 0;

    case WM_DESTROY:
        DiscardGraphicsResources();
        SafeRelease(&pFactory);
        KillTimer(m_hwnd, 1000);
        PostQuitMessage(0);
        return 0;

    case WM_PAINT:
        OnPaint();
        return 0;

    case WM_TIMER:
        count = (count + 1) % 60;
        if (count == 59) {
            min1 = (min1 + 1) % 10;
            if (min1 == 0) {
                min2 = (min2 + 1) % 6;
                if (min2 == 0) {
                    if (h2 == 2) {
                        h1 = (h1 + 1) % 4;
                    }
                    else {
                        h1 = (h1 + 1) % 10;
                    }

                    if (h1 == 0) {
                        h2 = (h2 + 1) % 3;
                    }
                }
            }
        }
        show_dot = !show_dot;
        InvalidateRect(m_hwnd, nullptr, true);
        return 0;


    case WM_SIZE:
        Resize();
        return 0;
    }
    return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
}
